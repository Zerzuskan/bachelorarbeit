﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets;
using UnityEngine;
using Random = UnityEngine.Random;

public class TestInfoScript : AMutatable
{
    public TextMesh text;
    private float temperatureRange = 3;
    private float lifetime;
    private int RemainingCopulationIterations;
    private bool mutated;


    public float OptimalTemperature { get; set; } = 0;
    private float DeadRange { get; set; } = 3f;


    public override float Copulationrange { get; set; } = 10;
    

    void Start()
    {
        lifetime = Random.Range(3f, 6f);
        RemainingCopulationIterations = 6;
        if (transform.position.y > 40 | transform.position.y < -40)
        {
            gameObject.SetActive(false);
        }

        mutated = false;
    }

    void Update()
    {
        lifetime -= Time.deltaTime;
        text.text = OptimalTemperature.ToString("F1");

        if (lifetime <= 0)
        {
            List<AMutatable> findings = FindObjectsOfType<AMutatable>().ToList();
            findings.Remove(this);
            foreach (AMutatable f in findings)
            {
                if (Vector3.Distance(gameObject.transform.position, f.transform.position) < DeadRange)
                {
                    Debug.Log("Died: Deadrange");
                    Destroy(gameObject);
                    return;
                }
            }

            if (transform.position.x/5 > OptimalTemperature+temperatureRange | transform.position.x/5 < OptimalTemperature-temperatureRange) 
            {
                Debug.Log("Died: Temperature");
                Destroy(gameObject);
                return;
            }

            //if (!mutated)
            //{
            //    mutated = true;
            //    GetComponent<Mutator>().Mutate();
            //    text.text = OptimalTemperature.ToString("F1");
            //}
            gameObject.GetComponent<Mutator>().Copulate();
            gameObject.GetComponent<Mutator>().Copulate();
            if (RemainingCopulationIterations > 0)
            {
                RemainingCopulationIterations--;
                lifetime = 1;
                return;
            }
            Destroy(gameObject);
        }
    }
}