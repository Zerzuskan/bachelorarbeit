﻿using UnityEngine;

namespace Assets
{
    public abstract class AMutatable : MonoBehaviour
    {
        [HideInInspector]
        public AMutatable parentA;
        [HideInInspector]
        public AMutatable parentB;

        [HideInInspector]
        public abstract float Copulationrange { get; set; }

    }
}