﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManipulator : MonoBehaviour
{
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.M))
        {
            Time.timeScale *= 2;
            Debug.Log("New Timescale: " + Time.timeScale);
        }

        if (Input.GetKeyDown(KeyCode.N))
        {
            Time.timeScale /= 2;
            Debug.Log("New Timescale: " + Time.timeScale);
        }
    }
}
