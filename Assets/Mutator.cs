﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Assets;
using UnityEngine;
using Random = UnityEngine.Random;

public class Mutator : MonoBehaviour
{
    public AMutatable MutatableObject;

    public List<MutatableProperty> MutatableProperties;



    public void Start()
    {
        //Mutate();
    }

    [ContextMenu("Copulate")]
    public void Copulate()          //Copluate with random in range
    {
        List<AMutatable> findings = FindObjectsOfType<AMutatable>().ToList();

        findings.Remove(MutatableObject);
        for (var index = 0; index < findings.Count; index++)
        {
            AMutatable f = findings[index];
            if (Vector3.Distance(MutatableObject.transform.position, f.transform.position) >
                MutatableObject.Copulationrange)
            {
                findings.Remove(f);
            }
        }

        for (var index = 0; index < findings.Count; index++)
        {
            AMutatable f = findings[index];
            if (f.GetType() != MutatableObject.GetType())
            {
                findings.Remove(f);
            }
        }

        if (findings.Count != 0)
        {
            Copulate(findings[Random.Range(0, findings.Count)]);
        }
        //else { Debug.Log("No objects found during random copulation"); }
    }

    public void Copulate(AMutatable other)
    {
        if (other.GetType() == MutatableObject.GetType())
        {
            Vector3 position = new Vector3(transform.position.x + Random.Range(-5f,5f), transform.position.y + Random.Range(-5f, 10f), transform.position.z);
            AMutatable child = Instantiate(this.gameObject, position, Quaternion.identity).GetComponent<AMutatable>();
            child.parentA = MutatableObject;
            child.parentB = other;
            

            Mutator mutator = child.GetComponent<Mutator>();

            mutator.MutatableProperties = new List<MutatableProperty>(MutatableProperties);         //mix properties of both parents in child
            for (int i = 0; i < mutator.MutatableProperties.Count; i++)
            {
                if (Random.value < 0.5f)
                {
                    mutator.MutatableProperties[i] = other.GetComponent<Mutator>().MutatableProperties[i];
                }
                
                child.GetType().GetProperty(mutator.MutatableProperties[i].PropertyName).SetValue(child, MutatableObject.GetType().GetProperty(mutator.MutatableProperties[i].PropertyName).GetValue(MutatableObject));

            }

            mutator.Mutate();
        }
        else
        {
            throw new Exception($"{other.GetType()} cannot copulate with {MutatableObject.GetType()}.");
        }
    }


    public void Mutate()
    {
        foreach (MutatableProperty prop in MutatableProperties)
        {
            if (Random.Range(0f, 1f) <= prop.MutationProbability)
            {
                AnimationCurve curve = prop.MutationCurve;
                int keynumberincurve = prop.MutationCurve.length;
                float mutationmodifer = curve.Evaluate(Random.Range(curve.keys[0].time, curve.keys[keynumberincurve-1].time));
                //Debug.Log(mutationmodifer);
                PropertyInfo propinfo = MutatableObject.GetType().GetProperty(prop.PropertyName);          //https://stackoverflow.com/questions/6637679/reflection-get-attribute-name-and-value-on-property

                MutateValues(propinfo, mutationmodifer, false);

                foreach (Constraint constraint in prop.Constrains) 
                {
                    PropertyInfo constrPropInfo = MutatableObject.GetType().GetProperty(constraint.PropertyName);
                    MutateValues(constrPropInfo, mutationmodifer*constraint.Ratio, true);
                }
            }
        }
    }

    private void MutateValues(PropertyInfo valueToChange, float mutationmodifer, bool isConstraint) 
    {
        switch (valueToChange.GetValue(MutatableObject))                        //https://stackoverflow.com/questions/298976/is-there-a-better-alternative-than-this-to-switch-on-type
        {
            case float f:
                valueToChange.SetValue(MutatableObject, f+mutationmodifer);
                break;

            case int i:
                int temp2 = (int)(i * (1 + mutationmodifer));
                valueToChange.SetValue(MutatableObject, (int)(i * (1 + mutationmodifer)));
                break;

            case string s:
                Debug.Log("string is unsupported");
                break;

            default:
                Debug.Log($"Unsupported datatype : {nameof(valueToChange)}");
                break;
        }
    }
}

[Serializable]
public struct MutatableProperty
{
    public string PropertyName;
    [Range(0,1)]
    public float MutationProbability;
    public AnimationCurve MutationCurve;
    public List<Constraint> Constrains;
}

[Serializable]
public struct Constraint
{
    public string PropertyName;
    public float Ratio;
}