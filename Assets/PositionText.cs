﻿using UnityEngine;

public class PositionText : MonoBehaviour
{
    public TextMesh text;

    void Start()
    {
        text.text = (transform.position.x/5 ).ToString("F1");
    }
}
